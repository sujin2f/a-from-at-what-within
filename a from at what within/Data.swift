//
//  Data.swift
//  a from at what within
//
//  Created by Malik on 2018-03-21.
//  Copyright © 2018 Sujin. All rights reserved.
//

import Foundation

class Data {
    
    static let data = Data()
    
     // MARK: - Properties
    var newsType: [String] = {
        return ["CBC", "BBC", "RT International", "Mashable", "FOX News"]
        }()
    
    var newsTypeLinks: [String] = {
        return ["http://www.cbc.ca/cmlink/rss-topstories", "http://feeds.bbci.co.uk/news/rss.xml", "https://www.rt.com/rss/", "http://feeds.mashable.com/Mashable", "http://feeds.foxnews.com/foxnews/latest"]
    }()
    
    // MARK: - Accessors
   
}
