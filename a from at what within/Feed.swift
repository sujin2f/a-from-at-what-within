//
//  File.swift
//  a from at what within
//
//  Created by Malik on 2018-03-20.
//  Copyright © 2018 Sujin. All rights reserved.
//

import Foundation

class Feed {

    var newsType: String
    var title: String
    var link: String
    
    init(newsType: String, title: String, link: String) {
        self.newsType = newsType
        self.title = title
        self.link = link
    }
}
