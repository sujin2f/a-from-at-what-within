//
//  FeedTableViewController.swift
//  a from at what within
//
//  Created by Malik on 2018-03-20.
//  Copyright © 2018 Sujin. All rights reserved.
//

import UIKit
// Using FeedKit to parse the RSS: https://github.com/nmdias/FeedKit/
import FeedKit
import SwiftSoup
import MBProgressHUD

class FeedTableViewController: UITableViewController {

    var index: Int?
    var newsTypeLinks = Data.data.newsTypeLinks
    var feed: RSSFeed?
    var feeds = [Feed]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadFeedsByType()
        
        navigationItem.title = Data.data.newsType[index!]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feeds.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FeedTypeCell", for: indexPath)

        // Configure the cell...
        cell.textLabel?.text = feeds[indexPath.row].title

        return cell
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        let feedHTMLString = loadHTMLFromURL(feedURLString: feeds[(tableView.indexPathForSelectedRow?.row)!].link)
        
        self.showLoadingHUD()
        DispatchQueue.global(qos: .userInitiated).async {
            var parsedString = ""
            switch self.index {
            case 0:
                parsedString = self.parseHtmlCBC(html: feedHTMLString)
            case 1:
                parsedString = self.parseHtmlBBC(html: feedHTMLString)
            case 2:
                parsedString = self.parseHtmlRT(html: feedHTMLString)
            case 3:
                parsedString = self.parseHtmlMashable(html: feedHTMLString)
            case 4:
                parsedString = self.parseHtmlFox(html: feedHTMLString)
            default:
                print("Couldn't extract story from html")
            }
            
            DispatchQueue.main.async {
                let articleString = segue.destination as? QuizViewController
                articleString?.articleString = parsedString
                self.hideLoadingHUD()
                articleString?.updateView()
                print("works")
            }
        }
        
    }
 
    
    //MARK: Private Methods
    private func loadFeedsByType(){
        
        let feedURL = URL(string: newsTypeLinks[index!])!
        let parser = FeedParser(URL: feedURL)!
        
        // Parse asynchronously, not to block the UI.
        parser.parseAsync { [weak self] (result) in
            self?.feed = result.rssFeed
            
            guard let feed = result.rssFeed else {
                print(result.error!)
                return
            }

            for item in feed.items!{
                self?.feeds.append(Feed(newsType: Data.data.newsType[(self?.index!)!], title: item.title!, link: item.link!))
                print(item.link!)
            }
            // Then back to the Main thread to update the UI.
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
        
        // Parse synchronously, blocks the UI.
//        let result = parser.parse()
//        switch result {
//        case let .rss(feed):
//            guard let feed = result.rssFeed, result.isSuccess else {
//                print(result.error)
//                return
//            }
//            for item in feed.items!{
//                feeds.append(Feed(newsType: Data.data.newsType[index!], title: item.title!, link: item.link!))
//                print(item.link!)
//            }
//        default:
//            print("Couldn't load news")
//        }
    }
    
    // method to parse html from URL
    private func loadHTMLFromURL(feedURLString: String) -> String {
        guard let feedURL = URL(string: feedURLString) else {
            print("Error: \(feedURLString) doesn't seem to be a valid URL")
            return ""
        }
        
        do {
            let feedHTMLString = try String(contentsOf: feedURL)
            return feedHTMLString
        } catch let error {
            print("Error: \(error)")
        }
        
        return ""
    }

    private func parseHtmlCBC(html: String) -> String {
        let doc: Document = try! SwiftSoup.parse(html)
        
        do{
            let div: Element? = try doc.select("div.story").first()
            if let html = try div?.html() {
                return html
            }
            return ""
        }catch Exception.Error(let type, let message){
            print(message)
        }catch{
            print("error")
        }
        return ""
    }
    
    private func parseHtmlBBC(html: String) -> String{
        let doc: Document = try! SwiftSoup.parse(html)
        
        do{
            let div: Element? = try doc.select("div.story-body").first()
            if let html = try div?.html() {
                return html
            }
            return ""
        }catch Exception.Error(let type, let message){
            print(message)
        }catch{
            print("error")
        }
        return ""
    }
    
    private func parseHtmlRT(html: String) -> String {
        let doc: Document = try! SwiftSoup.parse(html)
        
        do{
            let div: Element? = try doc.select("div.article").first()
            if let html = try div?.html() {
                return html
            }
            return ""
        }catch Exception.Error(let type, let message){
            print(message)
        }catch{
            print("error")
        }
        return ""
    }
    
    private func parseHtmlMashable(html: String) -> String {
        let doc: Document = try! SwiftSoup.parse(html)
        
        do{
            let div: Element? = try doc.select("section.article-content").first()
            if let html = try div?.html() {
                return html
            }
            return ""
        }catch Exception.Error(let type, let message){
            print(message)
        }catch{
            print("error")
        }
        return ""
    }
    
    private func parseHtmlFox(html: String) -> String {
        let doc: Document = try! SwiftSoup.parse(html)
        
        do{
            let div: Element? = try doc.select("div.article-body").first()
            if let html = try div?.html() {
                return html
            }
            return ""
        }catch Exception.Error(let type, let message){
            print(message)
        }catch{
            print("error")
        }
        return ""
    }
    
    // methods to show/hide MBProgressHUD
    private func showLoadingHUD() {
        let hud = MBProgressHUD.showAdded(to: UIApplication.shared.keyWindow!, animated: true)
//        let hud = MBProgressHUD.showAdded(to: contentView, animated: true)
        hud.label.text = "Loading..."
    }
    
    private func hideLoadingHUD() {
        MBProgressHUD.hide(for: UIApplication.shared.keyWindow!, animated: true)
    }
}
