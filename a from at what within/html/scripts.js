// jQuery( document ).ready(function($) {
	var quiz = [];
	var prepositions = /(\sabove(\s|,)|\sacross(\s|,)|\safter(\s|,)|\sat(\s|,)|\saround(\s|,)|\sbefore(\s|,)|\sbehind(\s|,)|\sbelow(\s|,)|\sbeside(\s|,)|\sbetween(\s|,)|\sby(\s|,)|\sfor(\s|,)|\sfrom(\s|,)|\sin(\s|,)|\sinside(\s|,)|\sonto(\s|,)|\sof(\s|,)|\soff(\s|,)|\son(\s|,)|\sout(\s|,)|\sthrough(\s|,)|\sto(\s|,)|\sunder(\s|,)|\sup(\s|,)|\swith(\s|,))/gi;
	var result = 0;

	// Strip 'a' tags
	$("a").each(function() {
		var html = $(this).html();
		$(this).replaceWith(html);
	});

	$('[class*="read-more"], [data-ad-text], img, noscript, body script').remove();

	var body = $('#ios-content-wrapper').html();
	body = body.replace(prepositions, function(matched) {
		var postfix = ' ';

		if ( matched.includes(',') ) {
			postfix = ', '
		}

		var answer = matched.replace(/\s/g, '').replace(',', '');
		quiz.push(answer);
		return ' <input type="text" class="inp-quiz" answer="' + answer + '" />' + postfix;
	});
	$('#ios-content-wrapper').html(body);

	$('#btn-submit').click(function() {
		var $result = $('.result')[0];
		var count = $('.inp-quiz').length;

		$('.inp-quiz').each(function(idx, elm) {
			var $r = $($result).clone();
			$r.removeClass('hidden');

			if ( $(elm).val().toLowerCase().replace(/\s/g, '') == $(elm).attr('answer').toLowerCase().replace(/\s/g, '') ) {
				console.log($r);
				$r.addClass('collect');
				$r.html($(elm).attr('answer').toLowerCase().replace(/\s/g, ''));
				result ++;
			} else {
				$r.addClass('wrong');
				$r.html($(elm).attr('answer').toLowerCase().replace(/\s/g, ''));
			}

			$r.replaceAll(elm);
		});

		console.log(count);

		$('#result #numResult').html(result);
		$('#result #numCount').html(count);
		$('#result').removeClass('hidden');

		window.scrollTo(0, 0);
	});

	$('#btn-start-again').click(function() {
		var message = {
			startAgain: "startAgain",
			backTitle: ""
		};
		window.webkit.messageHandlers.ios.postMessage(message);
	});

	$('#btn-to-title').click(function() {
		var message = {
			startAgain: "",
			backTitle: "backTitle"
		};
		window.webkit.messageHandlers.ios.postMessage(message);
	});

// });
