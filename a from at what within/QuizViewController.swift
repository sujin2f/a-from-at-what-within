//
//  QuizViewController.swift
//  a from at what within
//
//  Created by Sujin on 2018-03-20.
//  Copyright © 2018 Sujin. All rights reserved.
//

import UIKit
import WebKit

class QuizViewController: UIViewController, WKUIDelegate, WKNavigationDelegate, WKScriptMessageHandler {
    var mainWebView: WKWebView?
    let userContentController = WKUserContentController()
    var articleString: String?
    var config: WKWebViewConfiguration?

    let htmlHead = """
    <!DOCTYPE html><html>
        <head>
            <title>Title of the document</title>
            <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\" />
        </head>

        <body style=\"display:none\">
            <div name=\"top\" id=\"result\" class=\"hidden\">
                <div>You\'ve got <span id=\"numResult\"></span>/<span id=\"numCount\"></span></div>
                <div>
                    <button id=\"btn-start-again\">Start Again</button>
                    <button id=\"btn-to-title\">Back to the Title Screen</button>
                </div>
            </div>

            <span class=\"result hidden\"></span>

            <div id="ios-content-wrapper">
    """
    let htmlTail = "</div><div id=\"btn-submit\"><button>Check the Answer</button></div></body></html>"

    func updateView() {
        if articleString == nil {
            return
        }

        mainWebView = WKWebView(frame: self.view.bounds, configuration: config!)
        self.view.addSubview(mainWebView!)
        
        mainWebView?.allowsBackForwardNavigationGestures = true
        mainWebView?.uiDelegate = self
        mainWebView?.navigationDelegate = self

        let frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height)
        mainWebView!.frame = frame
        
        let html = htmlHead + ( articleString ?? "" ) + htmlTail
        
        mainWebView?.loadHTMLString(html, baseURL: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        config = WKWebViewConfiguration()
        config!.userContentController = userContentController
        userContentController.add(self, name: "ios")
    }
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        let dict = message.body as! [String:AnyObject]
        let startAgain = dict["startAgain"] as! String
        let backTitle = dict["backTitle"] as! String
        
        print(startAgain)
        print(backTitle)
        
        if backTitle != "" {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "TitleScreen")
            self.present(newViewController, animated: true, completion: nil)
        }
        
        if startAgain != "" {
            updateView()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        insertContentsOfCSSFile(filename: "styles")
        insertContentsOfJSFile(filename: "jquery-3.3.1.min")
        insertContentsOfJSFile(filename: "scripts")
    }
    
    func insertContentsOfCSSFile(filename: String) {
        guard let path = Bundle.main.path(forResource: filename, ofType: "css") else { return }
        let cssString = try! String(contentsOfFile: path).trimmingCharacters(in: .whitespacesAndNewlines)
        let jsString = "var style = document.createElement('style'); style.innerHTML = '" + cssString + "'; document.getElementsByTagName('head')[0].appendChild(style);"
        
        mainWebView?.evaluateJavaScript(jsString) { (result, error) in
            if error != nil {
                print("Error CSS")
                print(result)
                print(jsString)
            }
        }
    }
    
    func insertContentsOfJSFile(filename: String) {
        guard let path = Bundle.main.path(forResource: filename, ofType: "js") else { return }
        let jsString = try! String(contentsOfFile: path).trimmingCharacters(in: .whitespacesAndNewlines)
        mainWebView?.evaluateJavaScript(jsString) { (result, error) in
            if error != nil {
                print("Error JS")
                print(result)
                print(jsString)
            }
        }
    }
    
    func testGetHtmlString() -> String? {
        guard let path = Bundle.main.path(forResource: "sample", ofType: "html") else { return nil }
        return try! String(contentsOfFile: path).trimmingCharacters(in: .whitespacesAndNewlines)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension QuizViewController: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return nil
    }
}
